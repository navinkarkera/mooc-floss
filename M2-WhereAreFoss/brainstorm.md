- for this module you can just go directly to the evaluation is you feel confortable
- what are prerequisites ?
  - command line interface for git ? Rémi Sharrock has a mooc for that.
- this MOOC is focused on CODE contribution not design, not documentation etc...
- it's possible to use git without the command line:
  - web interface with gitab+gitpod IDE (eclipse che) or github+codespace- careful with the cost
  - going beyond a single line edit will need at some point the command line interface -bash, powershell)
- containers are a great way to bootstrap contributions:
  - everything correctly configured for dev
  - for example, database + proper test data etc.
  - docker compose
- important concepts:
  - tree of versions
  - merge and merge request / pull and pull requests
  - distributed: multiples copies, clones, forks (navigation the archeology of forks)
  - clone/fork merge/pull : taking care of synonyms, vocabulary
- hosting ourselves a personalized gitlab for a specific introductory activity
  - LTI + OmniAuth bridge / SSO : possibility to link the LTI anonymous ID sent by edx ?
- look at Oh My Git! + other links in the chat
- quality verification automatic tests: reading the contribution guidelines
- discourage the no community project and no maintener projects: not in scope for the MOOC.
- navigation with the "network of forks" to search the "real" project. Be sure not to be on an unmaintained fork.
- workflow: explaining pushing and failing (well that's not the workflow)

## Project tips

Assignee: Xavier

- starting to search for a project to contribute to:
  - Filter them by programming languages you know or want to use
  - Filter them by activity. Try to see which one you see are active.
  - possible questions or FLOWCHART (steps and questions to ask yourself):
    - "What is the date for the last commit?
    - "Is there an answer in the pull requests that are open?"
    - "Is there an IRC channel?"
 - Where is the Git? What is the branch? Is it working? good fork or not ?
 - GOURCE: the video visualization of the history of commits
 - vim + gitgrab

### Mapping a project's locations

TODO: Points from other modules brainstorms sessions that are related to the current week, to integrate. This week is about "where?", but it is currently very focused on technical "locations", ie git/forges. There are lots of other types of locations, like chat, mailing lists, etc. which are important to map

* Public vs private places: identifying and differentiating between:
  * Public places - where we can communicate with other because we can access the discussions and participate in them
  * Private places - other parts of the project where discussions happen behind closed doors for whatever reason.
  * Example: Google on Android. They develop behind closed doors within Google and then release once a year. So there are a variety of reasons why you don't hear part of the conversations, and you have to be able to detect that. Otherwise, you talk to walls and it can be very frustrating.
* Even the public places where discussions happen can be intricate to map. There might be the issue tracker on GitLab, a forum besides that, the IRC channel, and weekly videocalls -- with different types of conversations in each. Until you discover that these places exist, you are completely out of those discussions.
* Synchronous vs asynchronous: this will impact on our ability to interact with people there, or be present when things happen, depending on the respective timezones of the project members
* Different types of contributions and subcommunities might also be using different types of places:
  * Documentation and code often don't live in the same repositories
  * Different subcommunities/projects might be located in different places, and one part of the community might not be aware of what the other part is working on, especially for large projects (Ubuntu, GNOME, etc.). It's important to locate the different locations we could go to, and that they match the portion of the project we are interested in.
  * A contribution to the core might target a very different place than a contribution to plugins -- the processes and people involved might also change significantly
    * For a plugin, simply publishing it in the plugins repository might not require much communication -- it might also not really be an "upstream" contribution, unless it is meant to become an official plugin
    * A contribution to the core will have a lot more scrutiny because the project will have to maintain it

* Also showcase the locations of the MOOC's project, and encourage the students to go visit them
