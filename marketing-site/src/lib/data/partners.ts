import {urls} from './urls';

interface Partner {
  bio?: string;
  logo: string;
  name: string;
  url: string;
}

const partners: Record<string, Partner> = {
  framasoft: {
    name: 'Framasoft',
    url: urls.framasoft,
    bio: `A popular education nonprofit in France, focused on promotion,
    dissemination and development of free software`,
    logo: 'framasoft.svg',
  },

  telecomParis: {
    name: 'Télécom Paris',
    url: urls.telecomParis,
    logo: 'telecom-paris.svg',
  },

  openCraft: {
    bio: `
      A FLOSS provider and one of the main contributors to
      the Open EdX project
    `,
    name: 'OpenCraft',
    url: urls.openCraft,
    logo: 'opencraft.svg',
  },

  openEdx: {
    name: 'Open EdX',
    logo: 'open-edx.svg',
    url: urls.openEdx,
  },

  openInfrastructureFoundation: {
    name: 'Open Infrastructure Foundation',
    logo: 'open-infrastructure-foundation.svg',
    url: urls.openInfrastructureFoundation,
  },

  openStack: {
    name: 'OpenStack',
    url: urls.openStack,
    bio: `A training program about the different ways of contributing to the
      OpenStack project`,
    logo: 'openstack.svg',
  },

  imt: {
    name: 'Institut Mines-Télécom',
    url: urls.imt,
    bio: `A public institution dedicated to higher education, research and innovation
      in engineering and digital technology`,
    logo: 'institut-mines-telecom.svg',
  },

  patrickAndLinaDrahiFoundation: {
    name: 'Patrick and Lina Drahi Foundation',
    url: urls.patrickAndLinaDrahiFoundation,
    logo: 'patrick-and-lina-drahi-foundation.svg',
  },
};

export {partners};
