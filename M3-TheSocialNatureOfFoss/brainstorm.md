# Module 3 - Brainstorm, Sorted by Section

* Module coordinator: TBD
* Length: TBD

Content:
* Text
* Audio & video

Activities goals:
* do something, instead of only reading or listening
* interaction between students, or students and mentors, or students and professors, or students and the outside world)
* that you can really do with a tool
* second type of activity evaluated and count for the certificate

Evaluation:
* MCQ
* Peer reviewed exercise?

## Intro

* This module is about who is involved in the FLOSS community, organizations, what licenses, what economic models they have, charities, companies, sustainability.
* Also on topic: CLA/DCO, "free work"

## The social nature of FLOSS (Xavier)

Convey the idea that the main obstacle to contribution is social and not "code" - convince potential contributors it's worth their time:
* Even if you're a driveby contributor, you'll need to understand what the reviewer of your code will expect, what they will ask of you, etc
* This can be seen as part of *networking* in general, applied to the context of contributions
* Your contribution time is not the only thing involved in a successful contribution : There is also time spent by reviewers, by people maintaining or updating the code, etc. 
* Interact ASAP, because you will need to be known from people when you'll submit your MR. If they know you for a longer time, it will be easier as the people will know you. 
* It will also be easier if you know people to align expectations (you will know more about the scope of the project, and about the expertise of people present in the project)

When entering a project, you are not in "your" world (see week1 game):
* you have to "submit" yourself to the reviewers and "accept" that there are reviews (which may vex you or that you may think pointless -- you will score points by following the rules)
* Parallel with a party: you're at someone's place to take part in the party, you'll follow the host rules until you're a long-time contributor :)

FIXME: To reconcile with the "project tips" section of module 1, where this is also discussed.

## Projects and actors

When contributing to a project, it is important to be aware of the actors on it. Indeed, ultimately it will be the people from the project who will be deciding whether the contributions will be merged. Understanding who does what and who decides what will be a key element to consider to ensure that the contributions are aligned with the wishes of the maintainers.

### Different types of actors associated with projects

The differents types of actors associated with a project are the following:

- Communities
- Organizations
- Individuals

The term **Communities** refers to all the people and organizations gathering around a project, either as users, contributors or value stakeholders. This includes:

- Passerby who are interested in the project;
- Users and customers of the projects, who might be only using it, but not be involved directly in the project's day to day life;
- Contributors, who work on the project. This includes developers but also non-dev contributors like designers, documentation writers, people who answer in the forums, or even managers.

An **Organization** refers to an entity that can centralize the legal aspects, like hiring or taking legal decisions. For example, bylaws and associations are organizations. A company is a type of organization. All projects does not have an organization.

People who are responsible for governing a project, including leading and maintaining it, are part of communities and/or organizations. The details will depend on the project, and on the level of formalization of the project governance.

### Identify the stakeholders

A good practice when contributing to a project is to identify the stakeholders. Indeed, the stakeholders are the people to talk to. Knowing who the decision makers are, and what are the criterias they will use to take those decisions will be key to ensuring that our own work will get approved. Good questions to ask yourself are "Who are the maintainers ?" and "What are their exact roles and organizations ?".

It is also important to identify the users and customers of the project. Taking the time to identify them allows us to better determine their needs and the very reason the project exists and gets worked on. Even if they aren't always part of all the decisions, they need to be kept in mind while working on contributions, or during discussions around changes to make to the project.

Some of the contributors or maintainers might be contributing on behalf of a company, which will also have its own motivations and needs to be met, and those will also influence decisions.

Identifying on which basis people contribute will also help you understand when you can interact with them: when dealing with people who contribute in their spare time, it will be in the evening their time, and during the weekends. With paid staff, they usually will only be around during their working hours.


* Present examples of relationships between the different roles:
  * Listing of projects
  * Listing of organizations
  * Listing of companies
  * Listing of maintainers and their roles
  * etc.

### Project vs organization

When contributing to a project, it is important to be aware of the actors on it.

* There are some simple cases where the organization and the free software project is the same thing. But often, there are many organizations and different stakeholders acting in projects.
* At the end, we contribute to a project. So it's important to be aware of the actors on it. They can be individuals, organizations, companies, etc.
* An organization can be around lots of projects. For instance, KDE can be seen as an organization even if it has several organizations on specific projects. They have some sort of suborganizations in organizations.
* A project can be either small or large, made of subprojects, independent or interdepedent with many other projects
* Examples:
  * OpenStack: an open consortium constitued by a lot of different companies contributing to the code base, and a community. 
  * Inkscape: an open community without any big company or firm that controls the source code.

### Activity

* This is done first for a project and/or organization we have selected, and for which the answers will be known and the same for everyone (part 2 at the end of the module, for the project the student selected to contribute to)
* Matching game - who's both in a company and contributing to several projects, or is there a single company managing several projects and so on.
* For a given project: find the name of the organizations, companies, or fiduciary umbrellas. Associate the people or organizations involved to categories (company, informal group, friends, fiduciary umbrella, etc.) which is often invisible.
* For a given organization: list their projects

## Licenses & economic models

### Licenses

* The main licenses and their differences, what they imply
  * Copyleft vs MIT
  * Difference between licences related to code (GPL, etc) vs licences about content (CC-By-XX)
* Define notions: free, nonfree, copyrighted, dual licensing, etc.
* Get experts on licenses to discuss them (via interviews):
  * Bradley M. Kuhn
  * With maybe infographics or animations
  * Provide diversity of opinions (get pro & cons from the different camps, don't favor specific point of views)
* https://choosealicense.com
* https://creativecommons.org/choose/
* Talk about recent changes of licenses from SaaS vendors (ElasticSearch, Kibana, etc.)

### Economic models

* Examples of business models:
  * Service-based, like Red Hat's where you can pay to have support or services or development around products
  * SaaS, software as a service. Ex: Discourse
  * Dual licensing
  * Open core

* Explain how economic models relate to licensing.
  * For example, why dual licensing an AGPL project would make a company pay to use the project under a different license

* Scarcity as a useful lens to analyse a business plan related to FLOSS:
  * Main criteria: does it accomodate the non-scarce nature of FLOSS (and electronic data in general), or does it attempt to recreate that scarcity artificially? Ie does it embrace the nature of FLOSS and electronic freedom, or does it attempt to hold it back to be able to fit more traditional business models?
  * A proprietary license is a good example of attempting to deal with software like physical goods -- by forbidding the redistribution, even if the act of copying wouldn't cost anything by itself. This recreates scarcity artificially: if you want to be able to copy the software, you need to pay something every time. 
  * On the other side, there will be things that are not artificial in terms of scarcity: services like the business model of Red Hat or OpenCraft. Or SaaS, which monetizes server resources, and maintenance. Or this course, which is freely distributable but if you want service, if you want mentorship, if you want things that can't be multiplied freely to infinity, then there is still a need to allocate scarce resources through monetary payments -- but without holding up what can be copied freely.
  * There will also be intermediary ones, like for example dual licensing. You can actually use it any way you want, but if you want to be able to put your own conditions, then you need to pay. So it's not actually something scarce if you play by the rules, but you can have a scarcity in terms of licensing terms which is a bit artificial.

### Impact on contributions

* Articulate what the interaction between business models imply for contribution: does it make it better or easier to contribute? Or harder? Advantages or disadvantages contributors?
* Provide real examples from various types of contributors:
  * Xavier would have stories to tell as a for-profit contributor to a non-profit project
  * Loic would have other stories to tell (about Red Hat?)
  * Independent contractors who contributed to Red Hat projects would also have interesting stories to tell
  * OpenStack: most of the participants were employees of companies who had financial interests in contributing to OpenStack. In this type of context, it's important to understand how the business model of our own company interacts with the business model of OpenStack as a foundation but also all the other stakeholders in OpenStack and who were the biggest influencers of the project. That can decide if their contribution had a chance to make it because it highly dependent on how influential their companies was to the company who was the lead of the project at the time.
* Impact of CLAs: amounts to signing away your rights, and makes the work susceptible to a relicensing, like with Elastic
* It's important to figure out the impact of the business plan of the project and its actors, and how it relates to you and your contributions. Otherwise you can end up in a situation where you have done everything right, but your contribution still gets nowhere -- just because it doesn't fit the business plan. It will often not be said explicitly in those cases -- people might remain friendly, but your contribution will be overlooked, and will be left unmerged for a long time, without explanation.
* Differentiating the people who make decisions from passerbys can be key -- sometimes the most chatty people will not be someone taking the actual decisions afterwards. Just following blindly what that person says might get us nowhere.

### Activity preparation: Dealing with trolls or disputes?

* Discussions about licenses and economic models will inevitably give trolls, like on SSPL. This section is to prepare learners for that aspect of the activity, and the trolls it would generate (which would then be a learning opportunity).
* As a new contributor in a project you can be confronted to them. It can be useful to be prepared to it, recognize trolls and know how to act
* Sociological/anthropological presentation about trolling in free software
  * Present the work of Gabriella Coleman (Quebec) https://media.ccc.de/v/36c3-10875-hack_curio
* Note: Trolls are very high on Bloom's Taxonomy https://cft.vanderbilt.edu/guides-sub-pages/blooms-taxonomy/ - You have to understand stuff very well to be able to invent traps into which people will fall to make fun of them.

### Activity

#### Case studies

* Case study to analyze some of the projects learners use:
  * From a given real example, learners have to figure out the license and economic model, and how they relate to each other
  * To help figuring out the relationships between organizations, licenses and business models intuitively and progressively, we could use prebuilt case studies where students only have to analyze some parts of them. Give some part of information, and learners deduct the logic behind it.
  * Start from existing cases, and ask the learners to take the position of the maintainer or organizations: "what is the best license option for this use case?"
  * Depending on individual philosophy, the conclusion of which license to use is going to be very different. Remaining neutral about whether that was right or not, just explain why those actors took those decision without judgement -- or, better, get someone from the project to explain it themselves.
  * Get the learner to compare their perspective with the one from the project -- and also with the perspective of the other students (show stats on the frequency of answers - use MCQs to allow to aggregate them)
  * Use it to show that besides rational thoughts, there are also philosophical differences that will influence how people look at those choices -- and it's important to be in a community where you agree with the choices and culture, as it will have many ramifications
  * Showcase a set of very different projects. Ex:
    * Google Project with CLAs
    * Inkscape project with GPL and not dual license

* Also take inspiration from the Business Model Canvas? A business is broken down in its componets: clients, strategy, etc. And based on the Business Model Canvas description of the strategy of a project, guess which one it is, and/or what its license is.

* Also assert your own situation, your own business model if you have one (or your company's), and for the contribution that you envision to make, how does it interact?

#### Reading and explaining a license

* Figure out how does the project license and contribution process will affect their contribution:
  * Write down whether there is a CLA, or whether the author keeps their copyright
  * Identify the license that will apply, then pair with someone else, Read one line of the license to the other, and explain it to them. And then the roles are reversed. 
  * To do maybe over videoconference, or as a forum activity? Or split the licenses into multiple parts and different parts are spread to different students.

* Alternative idea: Based on a scenario, or on reading the lines of a license like described above, the learner has to explain "What would be the question to ask if we want to have this license text?". And provide questions similar to the questions asked by the creative commons license selector (https://creativecommons.org/choose/).
  * Or come up with a case, invent a fake project and describe why that project would fit into this license.

#### Debate on the comparison of licenses?

TBD

## Project tips (Xavier)

* To expand our knowledge of the project and its actors, in this module the activity is one similar to the one done earlier in the module, but for the project that students have selected to contribute to. The information, not known in advance is then contributed to Wikipedia/Wikimedia.

### Dealing with rejection

* Warn that the contribution might be rejected -- Wikipedia can be harsh on its criterias for accepting contributions (yet it's the most well known, and it works despite of that, which puts things in perspective).
* When we are contributing to a common good where other people are governing, there is a review process to ensure contributions match the criteria from the people governing it (including post-moderation via reverts, in the case of Wikipedia). It's important to be prepared to see it discarded.
* In the case of Wikipedia, the more small and sourced the chance and the highest chances it can stay.
* Segway into talking about negative experiences in FLOSS, and getting harsh reactions from maintainers:
  * Contributions and feedback goes both ways, be prepared not to take personally some remarks, or reverts! Contributing is also exposing ourselves to others, who might have different perspectives. Other contributors or maintainers will review your contributions, and give their opinon about it. It is often not what we expect.
  * Other users, contributors and maintainers might not be nice, or communicate poorly. It's important to be able to detatch ourselves from our work, to avoid taking it personally, and instead aim to take what we can out of it, to improve the final result.
  
### Collecting information about the project's actors

* Like in the previous activity, find the name of the organizations, companies, or fiduciary umbrellas. Associate the people or organizations involved to categories (company, informal group, friends, fiduciary umbrella, etc.) and roles
* Get the information by:
  * Reading the pages in Wikipedia and/or Wikidata
  * Reading the project website and documentation
  * Reading and identifying the actors intervening on the different project locations identified in module 2 ("Where?")
  * Using search engines
* Prepare to improve the data read from Wikipedia/Wikidata, by looking at how the article in Wikipedia about the project and the related Wikidata was built, behind the scense:
  * How the article in Wikipedia has changed over time, who were the initial contributors to the discussions, what happens behind the scenes. Not only you consume the data from Wikipedia but you know how it works behind the scenes and how it is indeed a contribution platform.

### Contributing the information to Wikipedia/Wikidata

* Post the outcome in Wikipedia and/or Wikidata, thus doing another (light) contribution
* In both cases, provide a tutorial for how to modify and improve that data
  * Also link and/or reuse the Wikipedia MOOC, or integrate with it
?
* Provide a forum thread to ask questions about how to do the contribution, share impressions about the experience, and post screenshots/links. To not be isolated for this activity, lost alone on Wikipedia. 
  * Could also use the peer-review to provide support and feedback to the learners

* Wikidata specific:
  * Ontology (bottom-up approach): https://www.wikidata.org/wiki/Wikidata:WikiProject_Informatics/FLOSS - the infobox of Open edX in Wikipedia which are usually pulled from Wikidata.
    * To know more, talk to Fabian Suchanek who is working a lot on anthologies
  * Explain & provide sample wikidata queries to explore the existing data for a given project (or pull it and represent it for the student ourselves)
  * For every data entered, add a link to the source of the data

* Validation/grading:
  * Query in Wikidata and/or Wikipedia - retreiving the diff contributed by the learner?
  * Peer-reviewed? Would help with:
    * Grading
    * Moderating the contributions made to third party projects (include abuse reports during reviews?)
    * Provide support & advices to learners, so they aren't left alone
    * Putting learners in the shoes of an upstream reviewer, who receive the contributions of other learners
  * Integrate with the "journal of contributions"?

### TODO: Elements to reconcile with "project tips" from previous weeks

* Get learners to do a first contribution on Wikipedia earlier, to prepare them for this exercise, and as an early ice-breaker for the act of contributing and publishing openly
  * Improving any Wikipedia page, for example on a topic that they know well or are an expert of
  * Get learners to share the experience in the forums, as also an ice breaker for interactions within the course. It's easier to get people to talk about topics they are interested in.
  * Also move the section about the chance of getting a revert / negative maintainer feedback?

How to find the discussion channels where people interact ?
- identifying PLACES and ROLES is important
- you'll (probably) need to ask questions to get into the codebase and find where the stuff you'll fix it, then discuss how to get things accepted.

Explain level of difficulty of contributions (ordered by *social* time to interact):
* answer a question
* fix a typo in the doc
* fix a trivial bug
* ...
* reorganize the governance of the project (you're now de facto in the governance core team)

## TODO: Points mentioned to integrate/develop in other modules

* To match the promise of getting a commit merged: 
  * What is paramount there is to properly choose the project and the contribution.
  * Can be part of mentoring: 
    * Talk to them and ask them to pick a contribution in the project that they want. Any project. Most of the time, they find the contribution and the project.
    * The project is right most of the time. But the contribution most of the time is not right. It's too difficult, it's too old, it will not receive attention, etc.
    * It takes between 15 minutes to 1 hour for every student before the course.
    * Important criterias to pick the contribution:
      * Matches their skill
      * It should not be too difficult (might or might not be labeled easy contribution)
      * Recently created
      * Matches a need that someone cares about, and that person has the power to merge it (avoids the problem where a contribution does not make it because people don't care)
  * Or when there is no mentor, this work can be offloaded to the projects:
    * If the project has an active community, then getting in touch with the project and discussing is a way to find things that interest the project and can interest you.
    * By interacting, you give information about what you are able to do, what are your skills, and you will get in return from people from the community ideas about what they plan to do, what can be done or what is easy to do in the project.

* Mental health of contributors

* Have a questionnaire at the beginning of the course, to get people to clarify their expectations, and ensure that they are not disappointed. Tell them that it's normal if things don't go right, that it takes time, and they can't be sure that people are available and so on and so forth. The important is to learn in the process.

